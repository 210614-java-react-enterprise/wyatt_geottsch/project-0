package dev.wyatt;

import dev.wyatt.daos.CustomerDaoImpl;
import dev.wyatt.models.Account;
import dev.wyatt.models.Customer;
import dev.wyatt.prompts.*;

public class App {

    public static final boolean TERMINAL = true;
    public static final String DB = "local";

    /**
     * Program main entry point.
     *
     * @param args
     */
    public static void main (String[] args) {

        // main loop
        while (true) {

            if (Welcome.prompt() == 1) {

                // login path
                String[] credentials = Login.prompt();

                Customer c = null;
                try {

                    CustomerDaoImpl cd = new CustomerDaoImpl();
                    c = cd.customerWithUserName(credentials[0]);

                    if (!c.getPasswd().equals(credentials[1])) {

                        System.out.println("Invalid username and/or password.");
                        Prompt.delay(2);

                        continue;

                    }

                } catch (NullPointerException e) {

                    System.out.println("Invalid username and/or password.");
                    Prompt.delay(2);

                    continue;

                } finally {

                    if (c == null) {
                        continue;
                    }

                }

                if (Home.prompt() == 1) {

                    // manage accounts
                    Account a = Accounts.prompt(c.getUserId());
                    Transaction.prompt(a.getAccountId());

                } else {

                    // new account
                    NewAccount.prompt(c.getUserId());

                }

            } else {

                // register path
                Register.prompt();

            }

            Goodbye.message("");

        }

    }

}
