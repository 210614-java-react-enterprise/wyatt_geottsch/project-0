package dev.wyatt.models;

import java.time.LocalDate;
import java.util.Objects;

public class Customer {

    private int userId;
    private String email;
    private String userName;
    private String passwd;
    private String fullName;
    private LocalDate birthDate;

    public Customer() {
        super();
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return userId == customer.userId && email.equals(customer.email) && userName.equals(customer.userName) && passwd.equals(customer.passwd) && fullName.equals(customer.fullName) && birthDate.equals(customer.birthDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, email, userName, passwd, fullName, birthDate);
    }

    @Override
    public String toString() {
        return "Customer{" +
                "userId=" + userId +
                ", email='" + email + '\'' +
                ", userName='" + userName + '\'' +
                ", passwd='" + passwd + '\'' +
                ", fullName='" + fullName + '\'' +
                ", birthDate=" + birthDate +
                '}';
    }

}
