package dev.wyatt.models;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.Objects;

public class Tx {

    private int txId;
    private int accountId;
    private OffsetDateTime dateTime;
    private BigDecimal amount;

    public int getTxId() {
        return txId;
    }

    public void setTxId(int txId) {
        this.txId = txId;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public OffsetDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(OffsetDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tx tx = (Tx) o;
        return txId == tx.txId && accountId == tx.accountId && dateTime.equals(tx.dateTime) && amount.equals(tx.amount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(txId, accountId, dateTime, amount);
    }

    @Override
    public String toString() {
        return "Tx{" +
                "txId=" + txId +
                ", accountId=" + accountId +
                ", dateTime=" + dateTime +
                ", amount=" + amount +
                '}';
    }

}
