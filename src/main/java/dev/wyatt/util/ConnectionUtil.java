package dev.wyatt.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static dev.wyatt.App.DB;

public class ConnectionUtil {

    private static Connection connection;

    /**
     * Database connection settings.
     *
     * @return
     * @throws SQLException
     */
    public static Connection getConnection() throws SQLException {

        if(connection == null || connection.isClosed()) {

            if (DB.equals("h2")) {

                // connect to h2
                connection = DriverManager.getConnection("jdbc:h2:/tmp/h2test");

            } else if (DB.equals("aws")) {

                // connect to aws
                String url =  "jdbc:postgresql://trainingdb.c4dnn2jbi3sh.us-east-2.rds.amazonaws.com:5432/postgres";

                final String USERNAME = System.getenv("USERNAME_AWS");
                final String PASSWORD = System.getenv("PASSWORD_AWS");

                connection = DriverManager.getConnection(url, USERNAME, PASSWORD);

            } else if (DB.equals("local")) {

                // connect to local database
                String url = "jdbc:postgresql://localhost:5432/bankdb";

                final String USERNAME = System.getenv("USERNAME_LOCAL");
                final String PASSWORD = System.getenv("PASSWORD_LOCAL");

                connection = DriverManager.getConnection(url, USERNAME, PASSWORD);

            }

        }

        return connection;

    }

}
