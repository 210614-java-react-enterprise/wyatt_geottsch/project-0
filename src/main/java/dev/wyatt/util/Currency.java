package dev.wyatt.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class Currency {

    public static final String fmt = "$#,##0.00;-$#,##0.00";

    /**
     * Format for BigDecimal numbers.
     *
     * @param n
     * @return
     */
    public static String format(BigDecimal n) {

        DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
        df.applyPattern(fmt);
        return df.format(n);

    }

}
