package dev.wyatt.prompts;

public class Goodbye extends Prompt {

    /**
     * Goodbye message.
     * @param fullName
     */
    public static void message(String fullName) {

        clearScreen();

        System.out.println("  ____                 _ _                ");
        System.out.println(" / ___| ___   ___   __| | |__  _   _  ___ ");
        System.out.println("| |  _ / _ \\ / _ \\ / _` | '_ \\| | | |/ _ \\");
        System.out.println("| |_| | (_) | (_) | (_| | |_) | |_| |  __/");
        System.out.println(" \\____|\\___/ \\___/ \\__,_|_.__/ \\__, |\\___|");
        System.out.println("                               |___/      ");
//        System.out.println("Have a great day, " + fullName.split("\\s+")[0] + ".");

        delay(2);

        clearScreen();

    }

}
