package dev.wyatt.prompts;

import dev.wyatt.daos.AccountDaoImpl;

import java.math.BigDecimal;

public class NewAccount extends Prompt {

    /**
     * New account form.
     *
     * @param userId
     * @return
     */
    public static boolean prompt(int userId) {

        clearScreen();

        System.out.println("                                                       _   ");
        System.out.println(" _ __   _____      __   __ _  ___ ___ ___  _   _ _ __ | |_ ");
        System.out.println("| '_ \\ / _ \\ \\ /\\ / /  / _` |/ __/ __/ _ \\| | | | '_ \\| __|");
        System.out.println("| | | |  __/\\ V  V /  | (_| | (_| (_| (_) | |_| | | | | |_ ");
        System.out.println("|_| |_|\\___| \\_/\\_/    \\__,_|\\___\\___\\___/ \\__,_|_| |_|\\__|");
        System.out.println("");

        // loop account name
        String accountName = validString("Account name: ");
        BigDecimal amount = validAmount("Initial deposit: $");

        // insert account
        AccountDaoImpl ad = new AccountDaoImpl();
        if (ad.createAccount(userId, accountName, amount)) { return true; }

        return false;

    }

}
