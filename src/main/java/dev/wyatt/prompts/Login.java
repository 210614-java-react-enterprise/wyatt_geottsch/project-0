package dev.wyatt.prompts;

import static dev.wyatt.App.TERMINAL;

public class Login extends Prompt {

    /**
     * Login form.
     *
     * @return
     */
    public static String[] prompt() {

        clearScreen();

        System.out.println(" _             _       ");
        System.out.println("| | ___   __ _(_)_ __  ");
        System.out.println("| |/ _ \\ / _` | | '_ \\ ");
        System.out.println("| | (_) | (_| | | | | |");
        System.out.println("|_|\\___/ \\__, |_|_| |_|");
        System.out.println("         |___/         ");
        System.out.println("");

        String userName = validString("Username: ");
        String passwd;
        if (TERMINAL) {
            passwd = validPassword();
        } else {
            passwd = validString("Password: ");
        }

        String[] credentials = {userName, passwd};

        return credentials;

    }

}
