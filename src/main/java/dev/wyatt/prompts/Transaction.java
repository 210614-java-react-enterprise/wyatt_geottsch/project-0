package dev.wyatt.prompts;

import dev.wyatt.daos.TxDaoImpl;
import dev.wyatt.models.Tx;
import dev.wyatt.util.Currency;

import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class Transaction extends Prompt {

    /**
     * Transaction menu prompt.
     *
     * @param accountId
     * @return
     */
    public static boolean prompt (int accountId) {

        clearScreen();

        System.out.println(" _                                  _   _                 ");
        System.out.println("| |_ _ __ __ _ _ __  ___  __ _  ___| |_(_) ___  _ __  ___ ");
        System.out.println("| __| '__/ _` | '_ \\/ __|/ _` |/ __| __| |/ _ \\| '_ \\/ __|");
        System.out.println("| |_| | | (_| | | | \\__ \\ (_| | (__| |_| | (_) | | | \\__ \\");
        System.out.println(" \\__|_|  \\__,_|_| |_|___/\\__,_|\\___|\\__|_|\\___/|_| |_|___/");
        System.out.println("");

        TxDaoImpl td = new TxDaoImpl();

        // display transaction history
        List<Tx> txs = td.txsWithAccountId(accountId);
        DateTimeFormatter dtf = DateTimeFormatter.ISO_DATE_TIME;
        for (Tx tx : txs) {
            System.out.println(
//                "Datetime: " + dtf.format(tx.getDateTime()) +
//                ", Tx No.: " + tx.getTxId() +
//                ", Amount: " + Currency.format(tx.getAmount())
                dtf.format(tx.getDateTime()) +
                "       " +
                Currency.format(tx.getAmount())
            );
        }
        System.out.println("");

        // display balance
        if (txs.size() > 0) {
            System.out.println("Balance: " + Currency.format(td.totalOfAccountId(accountId)));
            System.out.println("");
        }

        // loop selection
        int selection = validIntLt("1) Deposit\n2) Withdraw\n\nselect: ", 2);

        // loop amount
        BigDecimal amount = validAmount("Amount: $");

        // insert transaction
        if (selection == 1) {
            // deposit

            if (td.transaction(accountId, amount)) {
                return true;
            }

        } else {
            // withdraw

            BigDecimal balance = td.totalOfAccountId(accountId);
            while (amount.compareTo(balance) > 0) {
                System.out.println("Insufficient funds.");
                amount = validAmount("Amount: $");
            }

            if (td.transaction(accountId, amount.negate())) {
                return true;
            }

        }

        return false;

    }

}
