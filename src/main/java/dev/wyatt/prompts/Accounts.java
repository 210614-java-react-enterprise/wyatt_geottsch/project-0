package dev.wyatt.prompts;

import dev.wyatt.daos.AccountDaoImpl;
import dev.wyatt.models.Account;

import java.util.List;

public class Accounts extends Prompt {

    /**
     * Accounts menu prompt.
     *
     * @param userId
     * @return
     */
    public static Account prompt (int userId) {

        clearScreen();

        System.out.println("                                 _       ");
        System.out.println("  __ _  ___ ___ ___  _   _ _ __ | |_ ___ ");
        System.out.println(" / _` |/ __/ __/ _ \\| | | | '_ \\| __/ __|");
        System.out.println("| (_| | (_| (_| (_) | |_| | | | | |_\\__ \\");
        System.out.println(" \\__,_|\\___\\___\\___/ \\__,_|_| |_|\\__|___/");
        System.out.println("");

        // display accounts
        AccountDaoImpl ad = new AccountDaoImpl();
        List<Account> acs = ad.accountsWithUserId(userId);
        for (int i = 0; i < acs.size(); i++) {
            System.out.println(
                (i + 1) + ") " + acs.get(i).getAccountName() + " (Account No. " + acs.get(i).getAccountId() + ")"
            );
        }
        System.out.println("");

        // loop selection
        int selection = validIntLt("select: ", acs.size());

        return acs.get(selection - 1);

    }

}
