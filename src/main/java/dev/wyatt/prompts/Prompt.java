package dev.wyatt.prompts;

import java.io.Console;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Prompt {

    /**
     * Delay seconds.
     *
     * @param s
     */
    public static void delay(int s) {

        try {
            TimeUnit.SECONDS.sleep(s);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    /**
     * Clear screen.
     */
    protected static void clearScreen() {

//        int lines = 50;
//        for(int i = 0; i < lines; i++) {
//            System.out.println("");
//        }

        System.out.print("\033[H\033[2J");
        System.out.flush();

    }

    /**
     * Loop until valid full name.
     *
     * @return
     */
    protected static String validFullName() {

        System.out.printf("First and last name: ");

//        String pats = ".+ +.+"; // doesn't catch all spaces
        String pats = "[A-Z][A-Za-z]+\\s[A-Z][A-Za-z]+";
        Pattern pat = Pattern.compile(pats);

        Scanner input = new Scanner(System.in);

        String o;
        while (true) {
            o = input.nextLine();
            Matcher mat = pat.matcher(o);

            while (!mat.matches()) {
                System.out.println("Invalid input.");
                System.out.printf("First and last name: ");

                o = input.nextLine();
                mat = pat.matcher(o);
            }

            if (o.length() <= 100) {
                break;
            }
        }

        return o;

    }

    /**
     * Loop until valid email.
     *
     * @return
     */
    protected static String validEmail() {

        System.out.printf("Email: ");

        Scanner input = new Scanner(System.in);
        String o = input.nextLine();

        String pats = ".+@.+\\..+";
        Pattern pat = Pattern.compile(pats);
        Matcher mat = pat.matcher(o);

        while (!mat.matches()) {
            System.out.println("Invalid input.");
            System.out.printf("Email: ");

            o = input.nextLine();
            mat = pat.matcher(o);
        }

        return o;

    }

    /**
     * Loop until valid string.
     *
     * @param p
     * @return
     */
    protected static String validString(String p) {

        System.out.printf(p);

        Scanner input = new Scanner(System.in);
        String o = input.nextLine();

        while (o.length() < 1 || o.length() > 100) {
            System.out.println("Invalid input.");
            System.out.printf(p);
            o = input.nextLine();
        }

        return o;

    }

    /**
     * Loop until valid date of birth.
     *
     * @return
     */
    protected static LocalDate validDob() {

        Scanner input = new Scanner(System.in);

        LocalDate dob = null;
        while (dob == null) {

            int y, m, d;

            System.out.printf("Year of Birth (yyyy): ");
            try {
                y = Integer.parseInt(input.nextLine());
            } catch (NumberFormatException e) {
                y = 0; // change this
            }

            System.out.printf("Month of Birth (1-12): ");
            try {
                m = Integer.parseInt(input.nextLine());
            } catch (NumberFormatException e) {
                m = 0;
            }

            System.out.printf("Day of Birth (1-31): ");
            try {
                d = Integer.parseInt(input.nextLine());
            } catch (NumberFormatException e) {
                d = 0;
            }

            try {
                dob = LocalDate.of(y, m, d);
            } catch (DateTimeException e) {
                System.out.println("Invalid date.");
                continue;
            }

            if (dob.compareTo(LocalDate.now()) > 0 || dob.compareTo(LocalDate.now()) < -200) {
                System.out.println("Invalid date.");
                dob = null;
            }

        }

        return dob;

    }

    /**
     * Loop until valid integer within range.
     *
     * @param p
     * @param r
     * @return
     */
    protected static int validIntLt (String p, int r) {

        Scanner s = new Scanner(System.in);

        int o = 0;
        while (true) {
            System.out.printf(p);
            try {
                o = Integer.parseInt(s.nextLine());
                if (o < 1 || o > r) {
                    System.out.println("Invalid selection.");
                    continue;
                }
                break;
            } catch (NumberFormatException e) {
                System.out.println("Invalid selection.");
            }
        }

        return o;

    }

    /**
     * Loop until valid amount.
     *
     * @param p
     * @return
     */
    protected static BigDecimal validAmount (String p){

        Scanner input = new Scanner(System.in);

        BigDecimal amount;
        while (true) {
            System.out.printf(p);
            try {
                amount = new BigDecimal(input.nextLine());
                if (amount.compareTo(new BigDecimal(0)) <= 0 ||
                    amount.compareTo(new BigDecimal(999999999)) > 0) {
                    System.out.println("Invalid input.");
                    continue;
                }
                break;
            } catch (NumberFormatException e) {
                System.out.println("Invalid input.");
            }
        }

        return amount.setScale(2, RoundingMode.DOWN);

    }

    /**
     * Loop until valid password.
     *
     * @return
     */
    protected static String validPassword() {

        Console cons = System.console();
        char[] pw = cons.readPassword("%s", "Password:");

        while (pw.length < 1) {
            System.out.println("Invalid input.");
            pw = cons.readPassword("%s", "Password:");
        }

        return new String(pw);

    }

}
