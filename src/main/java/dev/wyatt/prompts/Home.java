package dev.wyatt.prompts;

public class Home extends Prompt {

    /**
     * Home menu prompt.
     *
     * @return
     */
    public static int prompt() {

        while (true) {

            clearScreen();

            System.out.println(" _                          ");
            System.out.println("| |__   ___  _ __ ___   ___ ");
            System.out.println("| '_ \\ / _ \\| '_ ` _ \\ / _ \\");
            System.out.println("| | | | (_) | | | | | |  __/");
            System.out.println("|_| |_|\\___/|_| |_| |_|\\___|");
            System.out.println("");

            return validIntLt("1) Manage accounts\n2) New account\n\nselect: ", 2);

        }

    }

}
