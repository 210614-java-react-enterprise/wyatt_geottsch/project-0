package dev.wyatt.prompts;

import dev.wyatt.daos.CustomerDaoImpl;
import dev.wyatt.models.Account;
import dev.wyatt.models.Customer;
import dev.wyatt.models.Tx;

import static dev.wyatt.App.TERMINAL;

public class Register extends Prompt{

    /**
     * Registration form.
     */
    public static void prompt() {

        clearScreen();

        System.out.println("                _     _            ");
        System.out.println(" _ __ ___  __ _(_)___| |_ ___ _ __ ");
        System.out.println("| '__/ _ \\/ _` | / __| __/ _ \\ '__|");
        System.out.println("| | |  __/ (_| | \\__ \\ ||  __/ |   ");
        System.out.println("|_|  \\___|\\__, |_|___/\\__\\___|_|   ");
        System.out.println("          |___/                    ");
        System.out.println("");

        Customer c = new Customer();
//        c.setFullName(validString("Full Name: "));
        c.setFullName(validFullName());
        c.setBirthDate(validDob());
        c.setEmail(validEmail());
        c.setUserName(validString("Username: "));

        if (TERMINAL) {
            c.setPasswd(validPassword());
        } else {
            c.setPasswd(validString("Password: "));
        }

        Account a = new Account();
        a.setAccountName(validString("Account name: "));

        Tx t = new Tx();
        t.setAmount(validAmount("Initial deposit: $"));

        CustomerDaoImpl cd = new CustomerDaoImpl();
        while (!cd.createCustomer(c, a, t)) {
            System.out.println("Username already exists.");
            c.setUserName(validString("Username: "));
        }

    }

}
