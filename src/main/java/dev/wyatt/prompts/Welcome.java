package dev.wyatt.prompts;

public class Welcome extends Prompt {

    /**
     * Welcome menu prompt.
     *
     * @return
     */
    public static int prompt() {

        while (true) {

            clearScreen();

            System.out.println("              _                          ");
            System.out.println("__      _____| | ___ ___  _ __ ___   ___ ");
            System.out.println("\\ \\ /\\ / / _ \\ |/ __/ _ \\| '_ ` _ \\ / _ \\");
            System.out.println(" \\ V  V /  __/ | (_| (_) | | | | | |  __/");
            System.out.println("  \\_/\\_/ \\___|_|\\___\\___/|_| |_| |_|\\___|");
            System.out.println("");

            return validIntLt("1) Login\n2) Register\n\nselect: ", 2);

        }

    }

}
