package dev.wyatt.daos;

import dev.wyatt.models.Account;

import java.math.BigDecimal;
import java.util.List;

public interface AccountDao {

    public List<Account> accountsWithUserId(int userId);

    public boolean createAccount(int userId, String accountName, BigDecimal amount);

}
