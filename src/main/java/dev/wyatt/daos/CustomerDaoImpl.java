package dev.wyatt.daos;

import dev.wyatt.models.Account;
import dev.wyatt.models.Customer;
import dev.wyatt.models.Tx;
import dev.wyatt.util.ConnectionUtil;

import java.sql.*;
import java.time.LocalDate;

public class CustomerDaoImpl implements CustomerDao {

    /**
     * Get the customer associated with the username.
     *
     * @param u
     * @return
     */
    @Override
    public Customer customerWithUserName(String u) {

        String sql =
            "select ct.userId, userName, passwd, email, fullName, birthDate from customer_table ct " +
            "join user_table ut " +
                "on ut.userId = ct.userId " +
            "join password_table pt " +
                "on pt.userId = ct.userId " +
            "where userName = ?";

        try (
                Connection connection = ConnectionUtil.getConnection();
                PreparedStatement ps = connection.prepareStatement(sql);
            )
        {
            ps.setString(1, u);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Customer c = new Customer();
                c.setUserId(rs.getInt("userId"));
                c.setUserName(rs.getString("userName"));
                c.setPasswd(rs.getString("passwd"));
                c.setEmail(rs.getString("email"));
                c.setFullName(rs.getString("fullName"));
                c.setBirthDate(rs.getObject("birthDate", LocalDate.class));
                return c;
            } else {
                return null;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return null;

    }

    /**
     * Insert a new customer into the database with initial account and deposit
     *
     * @param c
     * @param a
     * @param t
     * @return
     */
    @Override
    public boolean createCustomer(Customer c, Account a, Tx t) {

        String sqla = "insert into user_table values (default, ?)";
        String sqlb = "select currval('user_table_userid_seq')";
        String sqlc = "insert into password_table values (?, ?)";
        String sqld = "insert into customer_table values (?, ?, ?, ?)";
        String sqle = "insert into account_table values (default, ?, ?)";
        String sqlf = "select currval('account_table_accountid_seq')";
        String sqlg = "insert into tx_table values (default, ?, now(), ?)";

        try
        (
            Connection connection = ConnectionUtil.getConnection();
            PreparedStatement psa = connection.prepareStatement(sqla);
            Statement psb = connection.createStatement();
            PreparedStatement psc = connection.prepareStatement(sqlc);
            PreparedStatement psd = connection.prepareStatement(sqld);
            PreparedStatement pse = connection.prepareStatement(sqle);
            Statement psf = connection.createStatement();
            PreparedStatement psg = connection.prepareStatement(sqlg);
        )
        {

            connection.setAutoCommit(false);

            // set username
            psa.setString(1, c.getUserName());
            psa.executeUpdate();

            // set userid
            ResultSet rs = psb.executeQuery(sqlb);
            if (rs.next()) {
                c.setUserId(rs.getInt("currval"));
            } else {
                return false;
            }

            // set password
            psc.setInt(1, c.getUserId());
            psc.setString(2, c.getPasswd());
            psc.executeUpdate();

            // set customer identity
            psd.setInt(1, c.getUserId());
            psd.setString(2, c.getEmail());
            psd.setString(3, c.getFullName());
            psd.setObject(4, c.getBirthDate());
            psd.executeUpdate();

            // initial account
            pse.setInt(1, c.getUserId());
            pse.setString(2, a.getAccountName());
            pse.executeUpdate();

            // set accountid
            rs = psf.executeQuery(sqlf);
            if (rs.next()) {
                a.setAccountId(rs.getInt("currval"));
            } else {
                return false;
            }

            // initial deposit
            psg.setInt(1, a.getAccountId());
            psg.setObject(2, t.getAmount());
            psg.executeUpdate();

            connection.commit();

//            if (connection != null) {
//
//                try {
//
//                    connection.rollback();
//
//                    return false;
//
//                } catch (SQLException throwables) {
//
//                    throwables.printStackTrace();
//
//                    return false;
//
//                }
//
//            }

        } catch (SQLException throwables) {

//            throwables.printStackTrace();

            return false;

        }

        return true;

    }

}
