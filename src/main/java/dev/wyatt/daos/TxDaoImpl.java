package dev.wyatt.daos;

import dev.wyatt.models.Tx;
import dev.wyatt.util.ConnectionUtil;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

public class TxDaoImpl implements TxDao {

    /**
     * Get all transactions belonging to the account ID.
     *
     * @param accountId
     * @return
     */
    @Override
    public List<Tx> txsWithAccountId(int accountId) {

        String sql = "select * from tx_table where accountId = ?";

        try (
                Connection connection = ConnectionUtil.getConnection();
                PreparedStatement ps = connection.prepareStatement(sql);
            )
        {

            ps.setInt(1, accountId);
            ResultSet rs = ps.executeQuery();

            List<Tx> txs = new ArrayList<>();
            while (rs.next()) {
                Tx tx = new Tx();
                tx.setAccountId(rs.getInt("accountId"));
                tx.setAmount(rs.getObject("amount", BigDecimal.class));
                tx.setDateTime(rs.getObject("dateAndTime", OffsetDateTime.class));
                tx.setTxId(rs.getInt("txId"));
                txs.add(tx);
            }

            return txs;

        } catch (SQLException throwables) {

            throwables.printStackTrace();

        }

        return null;

    }

    /**
     * Get the balance of the account with account ID.
     *
     * @param accountId
     * @return
     */
    @Override
    public BigDecimal totalOfAccountId(int accountId) {

        String sql = "select sum(amount) from tx_table where accountId = ?";

        try (
                Connection connection = ConnectionUtil.getConnection();
                PreparedStatement ps = connection.prepareStatement(sql);
            )
        {

            ps.setInt(1, accountId);
            ResultSet rs = ps.executeQuery();

            if (rs.next())  {

                return rs.getObject("sum", BigDecimal.class);

            } else {

                return null;

            }

        } catch (SQLException throwables) {

            throwables.printStackTrace();

        }

        return null;

    }

    /**
     * Insert a transaction into the database.
     *
     * @param accountId
     * @param amount
     * @return
     */
    @Override
    public boolean transaction(int accountId, BigDecimal amount) {

        String sql = "insert into tx_table values (default, ?, now(), ?)";

        try (
            Connection connection = ConnectionUtil.getConnection();
            PreparedStatement ps = connection.prepareStatement(sql);
        )
        {

            ps.setInt(1, accountId);
            ps.setObject(2, amount);
            ps.executeUpdate();

        } catch (SQLException throwables) {

            throwables.printStackTrace();

        }

        return false;

    }

}
