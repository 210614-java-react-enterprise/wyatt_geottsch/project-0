package dev.wyatt.daos;

import dev.wyatt.models.Account;
import dev.wyatt.util.ConnectionUtil;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AccountDaoImpl implements AccountDao {

    /**
     * Get the accounts associated with the user ID.
     *
     * @param userId
     * @return
     */
    @Override
    public List<Account> accountsWithUserId(int userId) {

        String sql = "select * from account_table where userId = ?";

        try (
                Connection connection = ConnectionUtil.getConnection();
                PreparedStatement ps = connection.prepareStatement(sql);
            )
        {

            ps.setInt(1, userId);
            ResultSet rs = ps.executeQuery();

            List<Account> acs = new ArrayList<>();
            while (rs.next()) {
                Account ac = new Account();
                ac.setAccountId(rs.getInt("accountId"));
                ac.setAccountName(rs.getString("accountName"));
                ac.setUserId(rs.getInt("userId"));
                acs.add(ac);
            }

            return acs;

        } catch (SQLException throwables) {

            throwables.printStackTrace();

        }

        return null;

    }

    /**
     * Create an account with an initial deposit.
     *
     * @param userId
     * @param accountName
     * @param amount
     * @return
     */
    @Override
    public boolean createAccount(int userId, String accountName, BigDecimal amount) {

        String sqla = "insert into account_table values (default, ?, ?)";
        String sqlb = "select currval('account_table_accountid_seq')";
        String sqlc = "insert into tx_table values (default, ?, now(), ?)";

        try (
                Connection con = ConnectionUtil.getConnection();
                PreparedStatement psa = con.prepareStatement(sqla);
                Statement psb = con.createStatement();
                PreparedStatement psc = con.prepareStatement(sqlc);
            )
        {

            con.setAutoCommit(false);

            psa.setInt(1, userId);
            psa.setString(2, accountName);
            psa.executeUpdate();

            ResultSet rs = psb.executeQuery(sqlb);
            int accountId;
            if (rs.next()) {
                accountId = rs.getInt("currval");
            } else {
                con.rollback();
                return false;
            }

            psc.setInt(1, accountId);
            psc.setObject(2, amount);
            psc.executeUpdate();

            con.commit();

        } catch (SQLException throwables) {

            throwables.printStackTrace();

        }

        return true;

    }

}
