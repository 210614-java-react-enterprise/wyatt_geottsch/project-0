package dev.wyatt.daos;

import dev.wyatt.models.Tx;

import java.math.BigDecimal;
import java.util.List;

public interface TxDao {

    public List<Tx> txsWithAccountId (int accountId);

    public BigDecimal totalOfAccountId (int accountId);

    public boolean transaction (int accountId, BigDecimal amount);

}
