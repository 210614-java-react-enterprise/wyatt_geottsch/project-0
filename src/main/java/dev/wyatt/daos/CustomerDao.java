package dev.wyatt.daos;

import dev.wyatt.models.Account;
import dev.wyatt.models.Customer;
import dev.wyatt.models.Tx;

public interface CustomerDao {

    public Customer customerWithUserName(String u);

    public boolean createCustomer( Customer c, Account a, Tx t );

}
