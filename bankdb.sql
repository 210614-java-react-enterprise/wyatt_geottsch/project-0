                     /* _ */
  /* ___ _ __ ___  __ _| |_ ___ */
 /* / __| '__/ _ \/ _` | __/ _ \ */
/* | (__| | |  __/ (_| | ||  __/ */
 /* \___|_|  \___|\__,_|\__\___| */

/*^*/
create table user_table (
    userId serial primary key not null unique,
    userName varchar(100) not null unique
);

create table password_table (
    userId integer references user_table not null unique,
    passwd varchar(100) not null
);

create table customer_table (
    userId integer references user_table not null unique,
    eMail varchar(100) not null unique,
    fullName varchar(100) not null,
    birthDate date not null
);

create table account_table (
    accountId serial primary key not null unique,
    userId integer references user_table not null,
    accountName varchar(100) not null
);

create table tx_table (
    txId serial primary key not null unique,
    accountId integer references account_table not null,
    dateAndTime timestamptz not null,
    amount numeric(12,2) not null
);
/*$*/

     /* _ */
  /* __| |_ __ ___  _ __ */
 /* / _` | '__/ _ \| '_ \ */
/* | (_| | | | (_) | |_) | */
 /* \__,_|_|  \___/| .__/ */
     /*            |_| */

/*^*/
drop table user_table cascade;
drop table password_table cascade;
drop table customer_table cascade;
drop table account_table cascade;
drop table tx_table cascade;
/*$*/

 /* _                     _ */
/* (_)_ __  ___  ___ _ __| |_ */
/* | | '_ \/ __|/ _ \ '__| __| */
/* | | | | \__ \  __/ |  | |_ */
/* |_|_| |_|___/\___|_|   \__| */

/*^users*/
insert into user_table values (default, 'wyattg');
insert into password_table values (
    (select userId from user_table where userName = 'wyattg'),
    'passw0rd'
);
insert into customer_table values (
    (select userId from user_table where userName = 'wyattg'),
    'wyatt@goettsch.xyz',
    'Wyatt Goettsch',
    '1990-06-08'
);

insert into user_table values (default, 'samj');
insert into password_table values (
    (select userId from user_table where userName = 'samj'),
    'snakes'
);
insert into customer_table values (
    (select userId from user_table where userName = 'samj'),
    'sam@jackson.com',
    'Samuel Jackson',
    '1970-01-01'
);

insert into user_table values (default, 'markw');
insert into password_table values (
    (select userId from user_table where userName = 'markw'),
    'pancakes'
);
insert into customer_table values (
    (select userId from user_table where userName = 'markw'),
    'mark@wahlberg.com',
    'Mark Wahlberg',
    '1970-01-01'
);

insert into user_table values (default, 'kenw');
insert into password_table values (
    (select userId from user_table where userName = 'kenw'),
    'banzai'
);
insert into customer_table values (
    (select userId from user_table where userName = 'kenw'),
    'ken@watanabe.com',
    'Ken Watanabe',
    '1960-01-01'
);

insert into user_table values (default, 'jackiec');
insert into password_table values (
    (select userId from user_table where userName = 'jackiec'),
    'mulan'
);
insert into customer_table values (
    (select userId from user_table where userName = 'jackiec'),
    'jackie@chan.com',
    'Jackie Chan',
    '1960-01-01'
);
/*$*/

/*^accounts*/
insert into account_table values (
    default,
    (select userId from user_table where userName = 'wyattg'),
    'savings'
);

insert into account_table values (
    default,
    (select userId from user_table where userName = 'wyattg'),
    'checking'
);

insert into account_table values (
    default,
    (select userId from user_table where userName = 'wyattg'),
    'retirement'
);

insert into account_table values (
    default,
    (select userId from user_table where userName = 'samj'),
    'checking'
);

insert into account_table values (
    default,
    (select userId from user_table where userName = 'markw'),
    'checking'
);

insert into account_table values (
    default,
    (select userId from user_table where userName = 'kenw'),
    'checking'
);

insert into account_table values (
    default,
    (select userId from user_table where userName = 'jackiec'),
    'checking'
);
/*$*/

/*^txs*/
insert into tx_table values (
    default,
    1,
    now(),
    10.00
);

insert into tx_table values (
    default,
    1,
    now(),
    50.00
);

insert into tx_table values (
    default,
    1,
    now(),
    -5.00
);

insert into tx_table values (
    default,
    2,
    now(),
    10.00
);

insert into tx_table values (
    default,
    3,
    now(),
    10.00
);

insert into tx_table values (
    default,
    4,
    now(),
    10.00
);

insert into tx_table values (
    default,
    5,
    now(),
    10.00
);

insert into tx_table values (
    default,
    6,
    now(),
    10.00
);

insert into tx_table values (
    default,
    7,
    now(),
    10.00
);
/*$*/

/*
vim: fdm=marker fmr=/*^,/*$
*/
